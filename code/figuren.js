let dataDomain = "https://tonfiguren.metamuffin.org";

hausEnter = () => document.getElementById("haus").src = dataDomain + "/static/haus.gif";
hausLeave = () => document.getElementById("haus").src = dataDomain + "/static/haus-static.png";
hausClick = () => window.location = "/";

clearContent = () => document.getElementById("content").innerHTML = "";

function appendBanner(src) {
	const content = document.getElementById("content");
	let ban = document.createElement("img");
	ban.className = "banner";
	ban.src = dataDomain + "/im/" + src;
	ban.alt = "Banner";
	content.appendChild(ban);

	console.log("new!");
}

// fuck this
function replaceWith(banner, items) {
	clearContent(); // clear main area
	appendBanner(banner);

	const content = document.getElementById("content");

	let desc_text = document.createElement("p");
	desc_text.className = "desc-info";
	desc_text.innerHTML = "(Zum Vergr&ouml;&szlig;ern bitte auf die Bilder klicken)";
	content.appendChild(desc_text);

	const table = document.createElement("table");

	for (let item of items) {
		let tr = document.createElement("tr");

		let im = document.createElement("img");
		let td_im = document.createElement("td");
		let td_labels = document.createElement("td");

		tr.onclick = () => window.location.hash = "_" + encodeURIComponent(item.img);

		tr.className = "item";
		im.className = "item-im";
		td_im.appendChild(im);

		im.src = dataDomain + "/im/" + item.img;
		im.alt = "Bild: " + item.img;
		for (let pair of item.labels) {
			let div = document.createElement("div");
			let p_desc = document.createElement("p");
			let p_price = document.createElement("p");

			p_desc.innerHTML = pair[0];
			p_price.innerHTML = pair[1];
			div.appendChild(p_desc);
			div.appendChild(p_price);

			td_labels.appendChild(div);
		}

		tr.appendChild(td_im);
		tr.appendChild(td_labels);

		table.appendChild(tr);
	}

	content.appendChild(table);

	let feet = document.createElement("img");
	feet.src = "footer.png";
	feet.onclick = () => window.scrollTo(0, 0);
	feet.style.cursor = "pointer";
	content.appendChild(feet)
}

function replaceDesc(banner, item) {
	clearContent();
	appendBanner(banner);

	const content = document.getElementById("content");
	let img = document.createElement("img");
	let desc = document.createElement("p");

	img.className = "big-img";
	img.src = dataDomain + "/im/" + item.img;

	console.log(item.labels);

	desc.innerHTML = item.desc || item.labels.map(x => x[0]).join(", ");

	content.appendChild(img);
	content.appendChild(desc);
}

function createSidebar() {
	const sidebar = document.getElementById("sidebar");

	for (let k in categories) {
		let div = document.createElement("div");
		let schaf = document.createElement("img");
		let label = document.createElement("p");
		div.className = "schaf";
		schaf.src = dataDomain + "/static/schaf-gelb.png";
		schaf.alt = ""; // maximize lighthouse score lol
		label.innerHTML = k;

		div.onclick = () => window.location.hash = k;

		div.appendChild(schaf);
		div.appendChild(label);
		sidebar.appendChild(div);
	}
}

function changeCategory(cat) {
	console.log("Changing category to " + cat);

	const sidebar = document.getElementById("sidebar");
	for (let i = 0; i < sidebar.children.length; i++) {
		const e = sidebar.children[i];
		if (!e.className.includes("schaf")) continue; // skip the home button

		// if this is the newly selected category, change the sheep source to the
		// black one
		if (e.children[1].innerHTML == cat) {
			e.className = "schaf schaf-sel";
			e.children[0].src = dataDomain + "/static/schaf-schwarz.png";
		}
		// else, if this is the old category, change the sheep source to the yellow one
		else if (e.className === "schaf schaf-sel") {
			e.className = "schaf";
			e.children[0].src = dataDomain + "/static/schaf-gelb.png";
		}
	}

	// execute the category-specific callback
	// (which does things like, for example, change the page contents)
	categories[cat]();
}

function changeDesc(id) {
	console.log("Changing description to " + id);
	descs[id]();
}

function onHashChange() {
	let hash = window.location.hash || "#Neuheiten"; // default category
	hash = hash.substring(1); // remove leading '#'

	if (hash[0] !== "_") {
		// no zoomed in thingy; just a category
		changeCategory(decodeURIComponent(hash));
	}
	else {
		// verbose description of item
		changeDesc(hash.substring(1)); // remove leading "_"
	}
}

// ----------------------------------------------------------------------------
// config, initialization

var categories = {}; // callbacks for changing categories
var descs = {}; // callbacks for opening verbose descriptions of items
categories["Home"] = () => window.location.replace("/");

fetch(dataDomain + "/categories.json")
.then(res => res.json())
.then(cats => {
	// callback spaghett:
	// clicking things changes the window hash, and onHashChange reads the new
	// window hash, and runs either "changeCategory" or "changeDesc", which do
	// common work and then call specific code from the "categories" and "descs"
	// global objects, specifically.
	// window hashes are either category names or image links.

	for (let cat of cats) {
		console.log(cat.name)
		categories[cat.name] = () => replaceWith(cat.banner, cat.items);

		for (let item of cat.items) {
			let img_id = encodeURIComponent(item.img);
			descs[img_id] = () => replaceDesc(cat.banner, item);
		}
	}

	createSidebar();
	onHashChange(); // initial setup
})
.catch(err => {
	console.error(dataDomain + " is probably down/broken; error: " + err)
	console.error("TODO: redirecto to something useful.")
});

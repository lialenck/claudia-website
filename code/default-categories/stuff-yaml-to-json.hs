import Data.Aeson
import qualified Data.ByteString.Lazy as BL

main = do
    (Just ob) <- decodeFileStrict "stuff.json" :: IO (Maybe Value)
    BL.writeFile "minified-stuff.json" $ encode ob
